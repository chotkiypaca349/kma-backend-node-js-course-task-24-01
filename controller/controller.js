const JSJoda = require('@js-joda/core');
const LocalDate = JSJoda.LocalDate;

const isLeap = (year) => {
    return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0)
}

const dayDifference = (start, end) => {
    const startDate = LocalDate.parse(start.toISOString().slice(0, 10));
    const endDate = LocalDate.parse(end.toISOString().slice(0, 10));

    return JSJoda.ChronoUnit.DAYS.between(startDate, endDate);
}

class Controller {
    square(req, res) {
        const input = Number(req.body)

        const result = {
            number: input,
            square: input * input
        }

        return res.status(200).json(result)
    }

    reverse(req, res) {
        try {
            const input = req.body
            const reverseString = input.split('').reverse().join('')
            return res.status(200).type('text/plain').send(reverseString)
        } catch (e) {
            console.log(e)
            return res.status(400).json({message: 'bad request'})
        }
    }

    date(req, res) {
        try {
            const {year, month, day} = req.params
            const userDate = new Date(year, month - 1, day)
            const currDate = new Date()

            const weekDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]

            const result = {
                weekDay: weekDays[userDate.getDay()],
                isLeapYear: isLeap(year),
                difference: Math.abs(dayDifference(userDate, currDate))
            }

            return res.status(200).json(result)
        } catch (e) {
            console.log(e)
            return res.status(400).json({message: 'bad request'})
        }
    }
}


module.exports = new Controller()