const express = require('express')
const router = express.Router()
const controller = require('../controller/controller')


router.post('/square', controller.square)
router.post('/reverse', controller.reverse)
router.get('/date/:year/:month/:day', controller.date)

module.exports = router