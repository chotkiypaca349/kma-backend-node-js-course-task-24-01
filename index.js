const express = require('express')
const app = express()
const PORT = 56201
const router = require('./router/router')


app.use(express.text())
app.use(router)

app.listen(PORT, () => {
    console.log(`server is listening ${PORT} port`)
})
